ARG VERSION="latest"
FROM squidfunk/mkdocs-material:${VERSION}

ARG BUILD_DEPENDANCIES
ARG APK_PACKAGES 
ARG PIP_PACKAGES
ARG APK_MIRROR
ARG MKDOCS_THEMES

#RUN echo -e "\
#${APK_MIRROR}/v$(cat /etc/alpine-release | cut -f 1,2 -d '.')/main\n\
#${APK_MIRROR}/v$(cat /etc/alpine-release | cut -f 1,2 -d '.')/community" > /etc/apk/repositories

# Tweaked from squidfunk's dockerfile 
# Perform build and cleanup artifacts
RUN \
if [ ! -z "$APK_PACKAGES" ] || [ ! -z "$PIP_PACKAGES" ] || [ ! -z $MKDOCS_THEMES ]; then \
apk add --no-cache --virtual .build ${BUILD_DEPENDANCIES} && \ 
if [ ! -z "$APK_PACKAGES" ]; then \
    apk add --no-cache ${APK_PACKAGES}; \
fi && \
if [ ! -z "$MKDOCS_THEMES" ]; then \
    pip install --force-reinstall --no-cache-dir mkdocs; \
fi && \
if [ ! -z "$PIP_PACKAGES" ]; then \
    pip install --no-cache-dir ${PIP_PACKAGES}; \
fi && \
apk del .build && \
rm -rf /tmp/*; \
fi
